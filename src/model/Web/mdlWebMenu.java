package model.Web;

import java.util.List;

public class mdlWebMenu {
	public String menu_id;
	public String menu_name;
	public String menu_url;
	public String type;
	public String icon;
	public String menu_hierarki;
	public List<mdlWebMenu> sub;
}
