package model.Query;

import java.util.ArrayList;
import java.util.List;

public class mdlQueryTransaction {
	public String sql;
	public List<mdlQueryExecute> list_param = new ArrayList<mdlQueryExecute>();

	public String get_sql() {
		return sql;
	}
	public void set_sql(String sql) {
		this.sql = sql;
	}
	public List<mdlQueryExecute> get_list_param() {
		return list_param;
	}
	public void setListParam(List<mdlQueryExecute> listParam) {
		this.list_param = listParam;
	}
	
}
