package model.Result;

public class mdlUserIDParam extends mdlGetPaginationListParam {
	public String user_id;
	public String customer_id;
	public String merchant_id;
	public String distributor_id;
	public String product_id;
	public String type_id;
	public String email;
	public boolean status;
}
