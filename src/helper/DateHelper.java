package helper;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.IsoFields;
import java.util.Date;

public class DateHelper {

	public static String GetDateTimeNow(){
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		return dateFormat.format(date);
	}
	
	public static String GetDateTimeFormatter(String dateString){
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return dateFormat.format(dateString);
	}
	
	public static String GetDateTimeNowCustomFormat(String format){
		DateFormat dateFormat = new SimpleDateFormat(format);
		Date date = new Date();
		
		return dateFormat.format(date);
	}
	
	public static int GetIso8601WeekOfYear(String dateString)
    {
		int dayOfWeekIntValue;
		int weekOfYear;
		// ISO Date
	    LocalDate localDate = LocalDate.parse(dateString.split(" ")[0] ,DateTimeFormatter.ISO_LOCAL_DATE);
	    DayOfWeek dayOfWeek = localDate.getDayOfWeek();
	    dayOfWeekIntValue = dayOfWeek.getValue();
	    
	    //1:Monday--> 7: Sunday. belum pake Emum...
	    if(dayOfWeekIntValue>=1 && dayOfWeekIntValue<=3) {
	    	localDate=localDate.plusDays(3);
	    }
	    
	    weekOfYear = localDate.get( IsoFields.WEEK_OF_WEEK_BASED_YEAR ) ;
	    return weekOfYear; 
    }
}
