package helper;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

public class JsonHelper {

    public static String ConvertStringToJsonArray(JsonArray values) {
        String convertedObject = new Gson().toJson(values);
        return convertedObject;
    }

    public static JsonObject ConvertStringToJson(String values) {
        JsonObject convertedObject = new Gson().fromJson(values, JsonObject.class);
        return convertedObject;
    }

    public static String ConvertJsonToString(JsonObject values) {
        String convertedObject = new Gson().toJson(values, JsonObject.class);
        return convertedObject;
    }

}
