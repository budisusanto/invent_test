package helper;

public class NullHelper {
	public static String NulltoStringEmpty(String lParam){
		if (lParam == null) {
			lParam = "";
		}
		return lParam;
	}
	
	public static String[] NulltoStringArrayEmpty(String[] lParam){
		if (lParam == null) {
			lParam = new String[0];
		}
		
		return lParam;
	}
	
	public static String NulltoDateTimeEmpty(String lParam){
		if (lParam == null) {
			lParam = "0000-00-00 00:00:00";
		}
		
		return lParam;
	}

	public static String NulltoTimeEmpty(String lParam){
		if (lParam == null) {
			lParam = "00:00:00";
		}

		return lParam;
	}
	
	public static Integer NulltoIntEmpty(Integer lParam){
		if (lParam == null) {
			lParam = 0;
		}
		
		return lParam;
	}

	public static String NulltoZero(String lParam){
		if (lParam == null) {
			lParam = "0";
		}

		return lParam;
	}
	
}