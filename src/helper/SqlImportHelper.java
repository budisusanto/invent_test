package helper;

import model.Result.mdlResultSqlErrorRespon;

public class SqlImportHelper {
	public static mdlResultSqlErrorRespon ErrorMessageToMsg(String lParam){
		mdlResultSqlErrorRespon _mdlResultSqlErrorRespon = new mdlResultSqlErrorRespon();
		String _Result = "";
		if (lParam.contains("conflicted with the FOREIGN KEY")) {
			_mdlResultSqlErrorRespon.message_ind = "Error : terjadi konflik key data";
			_mdlResultSqlErrorRespon.message_eng = "Error : conflicted with key data";
		}
		else if (lParam.contains("duplicate key")){
			_mdlResultSqlErrorRespon.message_ind = "Error : terjadi duplikasi key data";
			_mdlResultSqlErrorRespon.message_eng = "Error : duplicate key data";
		}
		else if (lParam.contains("is too long. Maximum length is 128")) {
			_mdlResultSqlErrorRespon.message_ind = "Error : data terlalu panjang maximum 128";
			_mdlResultSqlErrorRespon.message_eng = "Error : data is too long maximum 128";
		}
		else if (lParam.contains("String or binary data would be truncated")) {
			_mdlResultSqlErrorRespon.message_ind = "Error : data terlalu panjang";
			_mdlResultSqlErrorRespon.message_eng = "Error : data is too long";
		}


		return _mdlResultSqlErrorRespon;
	}
	
}