package controller.v1;

import adapter.ProductAdapter;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import core.AuthorizationAdapter;
import core.ErrorSchemaAdapter;
import model.DataTable.mdlDataTableParam;
import model.DataTable.mdlDataTableResult;
import model.ErrorSchema.mdlErrorSchema;
import model.Globals;
import model.Product.mdlProduct;
import model.Result.mdlResultFinal;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/v1")
public class ProductController {

    @RequestMapping(value = "/get-product", method = RequestMethod.POST)
    public @ResponseBody
    mdlResultFinal GetProduct(
//            @RequestHeader String authorization,
//            @RequestHeader String key,
//            @RequestHeader String timestamp,
//            @RequestHeader String signature,
            @RequestBody String param,
            HttpServletResponse response){

        mdlResultFinal _mdlResultFinal = new mdlResultFinal();
        mdlErrorSchema _mdlErrorSchema = new mdlErrorSchema();
        GsonBuilder builder = new GsonBuilder();
        builder.serializeNulls();
        Gson gson = builder.create();

        String apiMethod = "POST";
        String url = "/get-product";

        mdlDataTableParam paramResult;
        paramResult = gson.fromJson(param, mdlDataTableParam.class);

//        _mdlResultFinal = AuthorizationAdapter.CheckAuthorization_v2(authorization, key, url, timestamp, signature, apiMethod, param); //only-live
        _mdlResultFinal.status_code = "ERR-00-000"; //only-testing

        if(_mdlResultFinal.status_code.equals("ERR-00-000")) {

            Globals.db_name = "invent"; //only-testing
//            Globals.dbName = ((mdlDB) mdlResultFinal.value).dbID; //only-live

            List<mdlProduct> _mdlProductList = ProductAdapter.GetProductList(paramResult);
            int _mdlProductListTotal = ProductAdapter.GetTotalProduct(paramResult);

            if (_mdlProductList == null || _mdlProductList.size() == 0) {
                //mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_99_999);

                mdlDataTableResult _mdlDataTableResult = new mdlDataTableResult();
                _mdlDataTableResult.records_total = 0;
                _mdlDataTableResult.records_filtered = 0;
                _mdlDataTableResult.draw = paramResult.draw+ 1;
                _mdlDataTableResult.data = new ArrayList<mdlProduct>() ;

                _mdlErrorSchema.error_code = "ERR-99-008";
                _mdlResultFinal = ErrorSchemaAdapter.GetErrorSchema(_mdlErrorSchema);
                _mdlResultFinal.value =  _mdlDataTableResult;

                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                //logger.info("FAILED = API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
            } else {
                //mdlAPIResult.error_schema = ErrorAdapter.GetErrorSchema(ErrorStatus.ERR_00_000);

                mdlDataTableResult _mdlDataTableResult = new mdlDataTableResult();
                _mdlDataTableResult.records_total = _mdlProductListTotal;
                _mdlDataTableResult.records_filtered = _mdlProductListTotal;
                _mdlDataTableResult.draw = paramResult.draw+ 1;
                _mdlDataTableResult.data = _mdlProductList;


                _mdlErrorSchema.error_code = "ERR-00-000";
                _mdlResultFinal = ErrorSchemaAdapter.GetErrorSchema(_mdlErrorSchema);
                _mdlResultFinal.value =  _mdlDataTableResult;

                response.setStatus(HttpServletResponse.SC_OK);
                //logger.info("SUCCESS. API : " + url + ", method : " + apiMethod + ", jsonIn :" + jsonIn + ", jsonOut : " + jsonOut);
            }

            return _mdlResultFinal;
        }
        else {
            return _mdlResultFinal;
        }
    }
}
