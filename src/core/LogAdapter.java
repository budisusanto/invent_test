package core;

import database.QueryAdapter;
import model.Query.mdlQueryExecute;

import java.util.ArrayList;
import java.util.List;

public class LogAdapter {
	public static void InsertLogExc(String lException,String lKey,String lQuery, String lCreated_by){
		model.Result.mdlResult result = new model.Result.mdlResult();
		List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
		//String Query = "INSERT INTO log_exception (log_exception, log_query, log_function, log_created_on, log_created_by) VALUES(?, ?, ?, ?, ?);";
		String Query = "{call sp_log_exception_insert(?, ?, ?, ?)}";
//		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//		Date Created_On = new Date();
//		String lCreated_On = df.format(Created_On);

		try{
			if(lCreated_by == null || "".equals(lCreated_by)) {
				lCreated_by = "EMPTY";
			}

			listParam.add(QueryAdapter.QueryParam("string", lException));
			listParam.add(QueryAdapter.QueryParam("string", lQuery));
			listParam.add(QueryAdapter.QueryParam("string", lKey));
			//listParam.add(QueryAdapter.QueryParam("string", lCreated_On));
			listParam.add(QueryAdapter.QueryParam("string", lCreated_by));

			if(QueryAdapter.QueryManipulateNoLog(Query, listParam, "InsertLogExc", "LOG") == true) {
				result.status_code = "00";
				result.status_message="Insert Log sukses";
			}
			else {
				result.status_code = "01";
				result.status_message="Insert Log gagal";
			}
		}
		catch(Exception ex){
			System.out.println(ex);
			result.status_code = "01";
			result.status_message="Insert Log gagal";
		}
	}

	public static void InsertLogExcWithDB(String lException,String lKey,String lQuery, String lCreated_by, String db){
		model.Result.mdlResult result = new model.Result.mdlResult();
		List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
		//String Query = "INSERT INTO log_exception (log_exception, log_query, log_function, log_created_on, log_created_by) VALUES(?, ?, ?, ?, ?);";
		String Query = "{call sp_log_exception_insert(?, ?, ?, ?)}";
//		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//		Date Created_On = new Date();
//		String lCreated_On = df.format(Created_On);

		try{
			if(lCreated_by == null || "".equals(lCreated_by)) {
				lCreated_by = "EMPTY";
			}

			listParam.add(QueryAdapter.QueryParam("string", lException));
			listParam.add(QueryAdapter.QueryParam("string", lQuery));
			listParam.add(QueryAdapter.QueryParam("string", lKey));
			//listParam.add(QueryAdapter.QueryParam("string", lCreated_On));
			listParam.add(QueryAdapter.QueryParam("string", lCreated_by));

			if(QueryAdapter.QueryManipulateNoLogWithDB(Query, listParam, "InsertLogExc", "LOG", db) == true) {
				result.status_code = "00";
				result.status_message="Insert Log sukses";
			}
			else {
				result.status_code = "01";
				result.status_message="Insert Log gagal";
			}
		}
		catch(Exception ex){
			System.out.println(ex);
			result.status_code = "01";
			result.status_message="Insert Log gagal";
		}
	}

	public static void InsertLogNotif(String laction, String lsend, String lrecieved, String lcreated_by) {
		model.Result.mdlResult result = new model.Result.mdlResult();
		List<mdlQueryExecute> listParam = new ArrayList();
		String Query = "{call sp_log_notif_insert(?,?,?,?)}";

		try {
			if (lcreated_by == null || "".equals(lcreated_by)) {
				lcreated_by = "EMPTY";
			}

			listParam.add(QueryAdapter.QueryParam("string", laction));
			listParam.add(QueryAdapter.QueryParam("string", lsend));
			listParam.add(QueryAdapter.QueryParam("string", lrecieved));
			listParam.add(QueryAdapter.QueryParam("string", lcreated_by));

			if (QueryAdapter.QueryManipulateNoLog(Query, listParam, "InsertLogExc", "LOG")) {
				result.status_code = "00";
				result.status_message = "Insert Log sukses";
			} else {
				result.status_code = "01";
				result.status_message = "Insert Log gagal";
			}
		} catch (Exception var8) {
			System.out.println(var8);
			result.status_code = "01";
			result.status_message = "Insert Log gagal";
		}

	}
}
