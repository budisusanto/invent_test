package core;

import model.ErrorSchema.mdlErrorSchema;
import model.Result.mdlResultFinal;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class AuthorizationAdapter {
    public static List<String> decryptAuthorizationKey(String authorization) {
		String clientIDclientSecret = Base64Adapter.DecryptBase64(authorization);
		String[] clientData = clientIDclientSecret.split(";");
		List<String> result = Arrays.stream(clientData).collect(Collectors.toList());
		return result;
    }

    public static String getAuthorizationKey(String ClientID, String ClientSecret) {
		String stringToSign = ClientID + ";" + ClientSecret;
				
		return Base64Adapter.EncryptBase64(stringToSign);
    }

    public static boolean CheckAuthorization(String authorization, String key, String url, String timestamp, String signature, String method, String param) {
    	try {
		    String[] authorizationList = authorization.split(" ");
		    String base64token = authorizationList[1];
		    String encryptedToken = Base64Adapter.DecryptBase64(base64token);
//		    String token = EncryptionAdapter.AES256Decrypt(encryptedToken, key);
			String token = EncryptAdapter.decrypt(encryptedToken, key);
		    String[] tokenData = token.split(";");
		    List<String> tokenDataList = Arrays.stream(tokenData).collect(Collectors.toList());
		    
		    if (tokenDataList.size() != 3) {
		    	return false;
		    } else {
				String authString = tokenDataList.get(0);
				String startDate = tokenDataList.get(1);
				int expiresIn = Integer.parseInt(tokenDataList.get(2));

				Date dateNow = new Date();
				DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
				Date dateToken = df.parse(startDate.replace("T", " "));
				long toSecond = 1000;
				int dateDiff = (int) (dateNow.getTime() - dateToken.getTime());
				int diffSecond = (int) (dateDiff / toSecond);
				System.out.println("diffSecond: "+diffSecond+ " expiresIn: "+expiresIn );
				if (diffSecond >= expiresIn) {
					return false;
				} else {
				    List<String> clientData = AuthorizationAdapter.decryptAuthorizationKey(authString);
				    String clientID = clientData.get(0);
				    String clientSecret = clientData.get(1);
				    System.out.println("clientID: "+clientID+ " clientSecret: "+clientSecret );
				    if (clientData.size() != 2 || clientID.equals("") || clientSecret.equals("")) {
				    	return false;
				    } else {
						List<String> appData = ClientAdapter.CheckClientID(clientID, clientSecret);
						String appName = appData.get(0);
						String apiKey = appData.get(1);
						System.out.println("appName: "+appName+ " apiKey: "+apiKey );
						if (appData.size() != 2 || appName.trim().equals("") || apiKey.equals("")) {
							return false;
						} else {
						    // check signature
						    // remove hashedBody in signature to make mobile generate signature easier
						    // String bodyToHash = jsonIn.replaceAll("\\s+", "");
						    // String hashedBody = SHA256Adapter.sha256(bodyToHash).toLowerCase();
						    // String stringToSign = method.toUpperCase() + ";" + url + ";" + base64token + ";" + hashedBody + ";" + timestamp;
						    // use signature without body
						    String stringToSign = method.toUpperCase() + ";" + url + ";"  + param + ";" +  base64token + ";" + timestamp;
						    System.out.println("String to sign 2 ");
						    System.out.println(stringToSign);
						    String apiSignature = EncryptAdapter.generateHmacSHA256Signature(stringToSign, apiKey);
						    System.out.println("apiSignature: "+apiSignature);
						    System.out.println("signature: "+signature);
						    if (!signature.equals(apiSignature)) {
						    	return false;
						    } else {
						    	return true;
						    }
						}
				    }
				}
		    }
		} catch (Exception ex) {
			System.out.println(ex.toString());
			return false;
		}
    }


	public static mdlResultFinal CheckAuthorization_v2(String authorization, String key, String url, String timestamp, String signature, String method, String param) {
		mdlErrorSchema mdlErrorSchema = new mdlErrorSchema();
		mdlResultFinal mdlResultFinal = new mdlResultFinal();
		try {
			String[] authorizationList = authorization.split(" ");
			String base64token = authorizationList[1];
			String encryptedToken = Base64Adapter.DecryptBase64(base64token);
			String token = EncryptAdapter.decrypt(encryptedToken, key);
			String[] tokenData = token.split(";");
			List<String> tokenDataList = Arrays.stream(tokenData).collect(Collectors.toList());

			if (tokenDataList.size() != 3) {
				mdlErrorSchema.error_code = "ERR-99-005";
				mdlResultFinal = ErrorSchemaAdapter.GetErrorSchema(mdlErrorSchema);
				return mdlResultFinal;
			} else {
				String authString = tokenDataList.get(0);
				String startDate = tokenDataList.get(1);
				int expiresIn = Integer.parseInt(tokenDataList.get(2));

				Date dateNow = new Date();
				DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
				Date dateToken = df.parse(startDate.replace("T", " "));
				long toSecond = 1000;
				int dateDiff = (int) (dateNow.getTime() - dateToken.getTime());
				int diffSecond = (int) (dateDiff / toSecond);
				System.out.println("diffSecond: " + diffSecond + " expiresIn: " + expiresIn);
				if (diffSecond >= expiresIn) {
					mdlErrorSchema.error_code = "ERR-99-006";
					mdlResultFinal = ErrorSchemaAdapter.GetErrorSchema(mdlErrorSchema);
					return mdlResultFinal;
				} else {
					List<String> clientData = AuthorizationAdapter.decryptAuthorizationKey(authString);
					String clientID = clientData.get(0);
					String clientSecret = clientData.get(1);
					System.out.println("clientID: " + clientID + " clientSecret: " + clientSecret);
					if (clientData.size() != 2 || clientID.equals("") || clientSecret.equals("")) {
						mdlErrorSchema.error_code = "ERR-99-005";
						mdlResultFinal = ErrorSchemaAdapter.GetErrorSchema(mdlErrorSchema);
						return mdlResultFinal;
					} else {
						List<String> appData = ClientAdapter.CheckClientID(clientID, clientSecret);
						String appName = appData.get(0);
						String apiKey = appData.get(1);
						System.out.println("appName: " + appName + " apiKey: " + apiKey);
						if (appData.size() != 2 || appName.trim().equals("") || apiKey.equals("")) {
							mdlErrorSchema.error_code = "ERR-99-005";
							mdlResultFinal = ErrorSchemaAdapter.GetErrorSchema(mdlErrorSchema);
							return mdlResultFinal;
						} else {
							// check signature
							// remove hashedBody in signature to make mobile generate signature easier
							// String bodyToHash = jsonIn.replaceAll("\\s+", "");
							// String hashedBody = SHA256Adapter.sha256(bodyToHash).toLowerCase();
							// String stringToSign = method.toUpperCase() + ";" + url + ";" + base64token + ";" + hashedBody + ";" + timestamp;
							// use signature without body

							//uncomment karena body spasi jangan di hilangkan
//							String bodyToHash = param.replaceAll("\\s+", "");
//							String stringToSign = method.toUpperCase() + ";" + url + ";" + bodyToHash + ";" + base64token + ";" + timestamp;
//
							String stringToSign = method.toUpperCase() + ";" + url + ";" + param + ";" + base64token + ";" + timestamp;

							System.out.println("String to sign 2 ");
							System.out.println(stringToSign);
							String apiSignature = EncryptAdapter.generateHmacSHA256Signature(stringToSign, apiKey);
							System.out.println("apiSignature: " + apiSignature);
							System.out.println("signature: " + signature);
							if (!signature.equals(apiSignature)) {
								mdlErrorSchema.error_code = "ERR-99-007";
								mdlResultFinal = ErrorSchemaAdapter.GetErrorSchema(mdlErrorSchema);
								return mdlResultFinal;
							} else {
								//Check db still valid or not
								model.Client.mdlDB mdlDB  = ClientAdapter.GetDbID(clientID,appName,"AuthToken");
								Date expiredDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(mdlDB.expired_date);

								Date date = new Date();

								if (!date.before(expiredDate)) {
									//buat error db sudah expired
									mdlErrorSchema.error_code = "ERR-99-009";
									mdlResultFinal = ErrorSchemaAdapter.GetErrorSchema(mdlErrorSchema);
									return mdlResultFinal;
								}

								mdlErrorSchema.error_code = "ERR-00-000";
								mdlResultFinal = ErrorSchemaAdapter.GetErrorSchema(mdlErrorSchema);
								mdlResultFinal.value = mdlDB;
								return mdlResultFinal;
							}
						}
					}
				}
			}
		} catch (Exception ex) {
			mdlErrorSchema.error_code = "ERR_00_000";
			mdlResultFinal = ErrorSchemaAdapter.GetErrorSchema(mdlErrorSchema);
			return mdlResultFinal;
		}
	}


}
