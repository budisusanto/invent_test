package core;

import model.ErrorSchema.mdlErrorSchema;
import model.ErrorSchema.mdlMessage;
import model.Query.mdlQueryExecute;
import model.Result.mdlResultFinal;

import javax.sql.rowset.CachedRowSet;
import java.util.ArrayList;
import java.util.List;

public class ErrorSchemaAdapter {

    public static mdlResultFinal GetErrorSchema(mdlErrorSchema mdlErrorSchema ) {
        CachedRowSet crs = null;
        mdlMessage mdlMessage = new mdlMessage();
        List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
        mdlResultFinal mdlResultFinal = new mdlResultFinal();
        String sql = "sp_get_error_schema ? ";
        try {
            listParam.add(database.QueryAdapter.QueryParam("string", mdlErrorSchema.error_code));
            crs = database.QueryAdapter.QueryExecute(sql, listParam, "GetErrorSchema", mdlErrorSchema.error_code);
            //test
            while (crs.next()) {
                mdlErrorSchema.error_code = crs.getString(1);
                mdlMessage.indonesian = crs.getString(2);
                mdlMessage.english = crs.getString(3);
                mdlErrorSchema.error_message = mdlMessage;
                mdlErrorSchema.error_api = crs.getString(4);
            }

            //mdlResultFinal.title  = mdlErrorSchema.errorApi;
            mdlResultFinal.status_code  = mdlErrorSchema.error_code;
            mdlResultFinal.status_message_ind  = mdlErrorSchema.error_message.indonesian;
            mdlResultFinal.status_message_eng  = mdlErrorSchema.error_message.english;
            mdlResultFinal.value = null;


        } catch (Exception ex) {
            mdlResultFinal.status_code = "ERR-99-999";
            mdlResultFinal.status_message_ind = "Sistem sedang tidak tersedia";
            mdlResultFinal.status_message_eng = "System not found";
            mdlResultFinal.value = null;
            System.out.println(ex);
            LogAdapter.InsertLogExc(ex.toString(), "GetErrorSchema", sql , "");
        }

        return mdlResultFinal;
    }

}
