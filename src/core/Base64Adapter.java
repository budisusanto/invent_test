package core;

import java.util.Base64;

public class Base64Adapter {
	public static String EncodeBase64Url(String input) {
		return Base64.getUrlEncoder().encodeToString(input.getBytes());
    }

    public static String DecodeBase64Url(String encoded) {
    	byte[] decodedURLBytes = Base64.getUrlDecoder().decode(encoded);

    	return new String(decodedURLBytes);
    }

	public static String EncryptBase64(String lKeyword){
		String lResult = "";
		try{
			byte[] encodedBytes = Base64.getEncoder().encode(lKeyword.getBytes());
			lResult = new String(encodedBytes);
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "EncryptBase64", "" , "");
		}
		return lResult;
	}
	
	public static String DecryptBase64(String lKeyword){
		String lResult = "";
		try{
			byte[] decodedBytes = Base64.getDecoder().decode(lKeyword);
			lResult = new String(decodedBytes);
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "EncryptBase64", "" , "");
		}
		return lResult;
	}
}
