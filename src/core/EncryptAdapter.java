package core;

import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.*;
import java.util.Base64;

public class EncryptAdapter {
	private final static String characterEncoding = "UTF-8";
	private final static String cipherTransformation = "AES/CBC/PKCS5Padding";
	private final static String aesEncryptionAlgorithm = "AES";

	public static  byte[] decrypt(byte[] cipherText, byte[] key, byte [] initialVector) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException
	{
	    Cipher cipher = Cipher.getInstance(cipherTransformation);
	    SecretKeySpec secretKeySpecy = new SecretKeySpec(key, aesEncryptionAlgorithm);
	    IvParameterSpec ivParameterSpec = new IvParameterSpec(initialVector);
	    cipher.init(Cipher.DECRYPT_MODE, secretKeySpecy, ivParameterSpec);
	    cipherText = cipher.doFinal(cipherText);
	    return cipherText;
	}

	public static byte[] encrypt(byte[] plainText, byte[] key, byte [] initialVector) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException
	{
	    Cipher cipher = Cipher.getInstance(cipherTransformation);
	    SecretKeySpec secretKeySpec = new SecretKeySpec(key, aesEncryptionAlgorithm);
	    IvParameterSpec ivParameterSpec = new IvParameterSpec(initialVector);
	    cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, ivParameterSpec);
	    plainText = cipher.doFinal(plainText);
	    return plainText;
	}

	private static byte[] getKeyBytes(String key) throws UnsupportedEncodingException{
	    byte[] keyBytes= new byte[16];
	    byte[] parameterKeyBytes= key.getBytes(characterEncoding);
	    System.arraycopy(parameterKeyBytes, 0, keyBytes, 0, Math.min(parameterKeyBytes.length, keyBytes.length));
	    return keyBytes;
	}

	public static String encrypt(String plainText, String key) throws UnsupportedEncodingException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException{
	    byte[] plainTextbytes = plainText.getBytes(characterEncoding);
	    byte[] keyBytes = getKeyBytes(key);
	    return Base64.getEncoder().encodeToString(encrypt(plainTextbytes,keyBytes, keyBytes));
	}

	public static String decrypt(String encryptedText, String key) throws KeyException, GeneralSecurityException, GeneralSecurityException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException, IOException{
	    byte[] cipheredBytes = Base64.getDecoder().decode(encryptedText);
	    byte[] keyBytes = getKeyBytes(key);
	    return new String(decrypt(cipheredBytes, keyBytes, keyBytes), characterEncoding);
	}

	public static String generateHmacSHA256Signature(String data, String key)
	// throws GeneralSecurityException, IOException
	{
		String algorithm = "HmacSHA256"; // OPTIONS= HmacSHA512, HmacSHA256,
		// HmacSHA1, HmacMD5
		String hash = "";
		try {
			// 1. Get an algorithm instance.
			Mac sha256_hmac = Mac.getInstance(algorithm);

			// 2. Create secret key.
			SecretKeySpec secret_key = new SecretKeySpec(key.getBytes("UTF-8"), algorithm);

			// 3. Assign secret key algorithm.
			sha256_hmac.init(secret_key);

			// Encode the string into bytes using utf-8 and digest it
			byte[] digest = sha256_hmac.doFinal(data.getBytes("UTF-8"));

			// Convert digest into a hex string
			hash = byteArrayToHex(digest);

		} catch (Exception e) {

		}
		return hash;
	}

	public static String byteArrayToHex(byte[] a) {
		StringBuilder sb = new StringBuilder(a.length * 2);
		for (byte b : a)
			sb.append(String.format("%02x", b));
		return sb.toString();
	}

}
