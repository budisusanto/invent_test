package core;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class EncryptionAdapter {
    public static String md5Convert(String text) throws NoSuchAlgorithmException{
        MessageDigest md = MessageDigest.getInstance("MD5");
        byte[] hashInBytes = md.digest(text.getBytes(StandardCharsets.UTF_8));

        StringBuilder sb = new StringBuilder();
        for(byte b : hashInBytes){
            sb.append(String.format("%02x", b));
        }

        return sb.toString();
    }

// Note : Tidak Di pakai lagi Karena Sudah Menggunakan Encrypt Adapter
//	private static byte[] hexkey = {
//		0x69, 0x6e, 0x76, 0x65, 0x6e, 0x74, 0x58, 0x58, 0x61, 0x6a, 0x61, 0x68, 0x2e, 0x30, 0x31, 0x33
//	};
//
//	public static String AES128Encrypt(model.Encryption.mdlEncryptionParam param){
//		String return_string = "";
//		try{
//			Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
//			SecretKeySpec secretKey = new SecretKeySpec(hexkey, "AES");
//			cipher.init(Cipher.ENCRYPT_MODE, secretKey);
//			byte[] cipherText = cipher.doFinal(param.stringContent.getBytes("UTF8"));
//			return_string = Base64.getEncoder().encodeToString(cipherText);
//		}
//		catch (Exception e){
//			e.toString();
//			return_string = "";
//		}
//		return return_string;
//	}
//
//	public static String AES128Decrypt(model.Encryption.mdlEncryptionParam param) {
//		String return_string = "";
//		try{
//			Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
//			SecretKeySpec secretKey = new SecretKeySpec(hexkey, "AES");
//			cipher.init(Cipher.DECRYPT_MODE, secretKey);
//			byte[] cipherText = Base64.getDecoder().decode(param.stringContent.getBytes("UTF8"));
//			return new String(cipher.doFinal(cipherText), "UTF-8");
//		}
//		catch (Exception e){
//			e.toString();
//			return_string = "";
//		}
//		return return_string;
//	}
//
//	private static String salt = "gajah.013";
//
//	private static byte[] getKeyBytes(String key) throws UnsupportedEncodingException{
//	    byte[] keyBytes= new byte[16];
//	    byte[] parameterKeyBytes= key.getBytes("UTF-8");
//	    System.arraycopy(parameterKeyBytes, 0, keyBytes, 0, Math.min(parameterKeyBytes.length, keyBytes.length));
//	    return keyBytes;
//	}
//
//	public static String AES256Encrypt(String StringContent, String SecretKey){
//		try{
//			IvParameterSpec initialVectorSpec = new IvParameterSpec(getKeyBytes(SecretKey) );
//			SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
//			KeySpec spec = new PBEKeySpec(SecretKey.toCharArray(), salt.getBytes(), 65536, 256);
//			SecretKey tmp = factory.generateSecret(spec);
//			SecretKeySpec secretKey = new SecretKeySpec(tmp.getEncoded(), "AES");
//
//			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
//			cipher.init(Cipher.ENCRYPT_MODE, secretKey, initialVectorSpec);
//			return Base64.getEncoder().encodeToString(cipher.doFinal(StringContent.getBytes("UTF-8") ) );
//		}
//		catch (Exception e){
//			return e.toString();
//		}
//	}
//
//	public static String AES256Decrypt(String StringContent, String SecretKey) {
//		try{
//			IvParameterSpec initialVectorSpec = new IvParameterSpec(getKeyBytes(SecretKey) );
//			SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
//			KeySpec spec = new PBEKeySpec(SecretKey.toCharArray(), salt.getBytes(), 65536, 256);
//			SecretKey tmp = factory.generateSecret(spec);
//			SecretKeySpec secretKey = new SecretKeySpec(tmp.getEncoded(), "AES");
//
//			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
//			cipher.init(Cipher.DECRYPT_MODE, secretKey, initialVectorSpec);
//			return new String(cipher.doFinal(Base64.getDecoder().decode(StringContent)));
//		}
//		catch (Exception e) {
//			return e.toString();
//		}
//	}
//
//	public static String sha256(String base) {
//		try {
//		    MessageDigest digest = MessageDigest.getInstance("SHA-256");
//		    byte[] hash = digest.digest(base.getBytes("UTF-8"));
//		    StringBuffer hexString = new StringBuffer();
//		    for (int i = 0; i < hash.length; i++) {
//			String hex = Integer.toHexString(0xff & hash[i]);
//			if (hex.length() == 1)
//				hexString.append('0');
//				hexString.append(hex);
//		    }
//		    return hexString.toString();
//		} catch (Exception ex) {
//			throw new RuntimeException(ex);
//		}
//    }
//
//    public static String generateHmacSHA256Signature(String data, String key){
//		String algorithm = "HmacSHA256"; // OPTIONS= HmacSHA512, HmacSHA256, HmacSHA1, HmacMD5
//		String hash = "";
//		try {
//		    Mac sha256_hmac = Mac.getInstance(algorithm);
//		    SecretKeySpec secret_key = new SecretKeySpec(key.getBytes("UTF-8"), algorithm);
//		    sha256_hmac.init(secret_key);
//		    byte[] digest = sha256_hmac.doFinal(data.getBytes("UTF-8"));
//		    hash = byteArrayToHex(digest);
//		} catch (Exception e) {
//
//		}
//		return hash;
//    }
//
//    public static String byteArrayToHex(byte[] a) {
//		StringBuilder sb = new StringBuilder(a.length * 2);
//		for (byte b : a)
//		    sb.append(String.format("%02x", b));
//		return sb.toString();
//    }

}
