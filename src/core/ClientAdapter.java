package core;

import database.QueryAdapter;

import javax.sql.rowset.CachedRowSet;
import java.util.ArrayList;
import java.util.List;

public class ClientAdapter {
    public static List<String> CheckClientID(String clientID, String clientSecret) {
        List<String> appData = new ArrayList<String>();
        List<model.Query.mdlQueryExecute> listParam = new ArrayList<model.Query.mdlQueryExecute>();
        CachedRowSet crs = null;
        String sql = "";
        appData.add("");
        appData.add("");
        try {
            sql = "SELECT app_name, api_key FROM mst_client_id WHERE client_id = ? AND client_secret = ? AND is_active = 1 ";
            listParam.add(QueryAdapter.QueryParam("string", clientID));
            listParam.add(QueryAdapter.QueryParam("string", clientSecret));
            crs = QueryAdapter.QueryExecute(sql, listParam, "CheckClientID", "AUTH_CHECK");
            while (crs.next()) {
                appData.set(0, crs.getString(1));
                appData.set(1, crs.getString(2));
            }
        } catch (Exception ex) {
            System.out.println(ex);
        }

        return appData;
    }

    public static model.Encryption.mdlKey CreateClientKeys(String appName) {
        model.Encryption.mdlKey mdlKey = new model.Encryption.mdlKey();
        String clientID, clientSecret, apiKey, clientID64, clientSecret64, apiKey64 = "";
        try {
            clientID = EncryptAdapter.encrypt(appName, "Invent123");
            clientSecret = EncryptAdapter.encrypt(clientID, appName);
            apiKey = EncryptAdapter.encrypt(clientSecret, clientID);

            clientID64 = Base64Adapter.EncryptBase64(clientID);
            clientSecret64 = Base64Adapter.EncryptBase64(clientSecret);
            apiKey64 = Base64Adapter.EncryptBase64(apiKey);

            mdlKey.app_name = appName;
            mdlKey.client_id = clientID64;
            mdlKey.client_secret = clientSecret64;
            mdlKey.api_key = apiKey64;

            String lkey = AuthorizationAdapter.getAuthorizationKey(clientID64,clientSecret64);

            System.out.println(lkey);

        } catch (Exception ex) {
            System.out.println(ex);
        }
        return mdlKey;
    }

    public static model.Client.mdlClient GetClientID(String clientID, String appCode, String user) {
        model.Client.mdlClient _mdlClient = new model.Client.mdlClient();
        List<model.Query.mdlQueryExecute> _mdlQueryExecuteList = new ArrayList<model.Query.mdlQueryExecute>();
        CachedRowSet crs = null;
        String sqlCommand = "";
        try {
            sqlCommand = "{call sp_client_get(?,?)}";
            _mdlQueryExecuteList.add(QueryAdapter.QueryParam("string", clientID));
            _mdlQueryExecuteList.add(QueryAdapter.QueryParam("string", appCode));

            crs = QueryAdapter.QueryExecute(sqlCommand, _mdlQueryExecuteList, "GetClientID", user);
            while (crs.next()) {
                _mdlClient.client_id = crs.getString("client_id");
                _mdlClient.client_secret = crs.getString("client_secret");
                _mdlClient.api_key =  crs.getString("api_key");
                _mdlClient.uri_image = crs.getString("uri_image");
                _mdlClient.module_id = crs.getString("module_id");

                _mdlClient.email = crs.getString("email");
                _mdlClient.phone = crs.getString("phone");
                _mdlClient.password = crs.getString("password");
                _mdlClient.server_key = crs.getString("server_key");
                _mdlClient.legacy_server_key = crs.getString("legacy_server_key");
                _mdlClient.web_api_key = crs.getString("web_api_key");
                _mdlClient.api_key_maps = crs.getString("api_key_maps");
            }

        } catch (Exception ex) {
            LogAdapter.InsertLogExc(ex.toString(), "GetClientID", sqlCommand, user);
            _mdlClient = null;
        }

        return _mdlClient;
    }

    public static model.Client.mdlDB GetDbID(String clientID, String appCode, String user) {
        model.Client.mdlDB mdlDB = new model.Client.mdlDB();
        List<model.Query.mdlQueryExecute> listParam = new ArrayList<model.Query.mdlQueryExecute>();
        CachedRowSet crs = null;
        String sqlCommand = "";
        try {
            sqlCommand = "{call sp_client_get(?,?)}";
            listParam.add(QueryAdapter.QueryParam("string", clientID));
            listParam.add(QueryAdapter.QueryParam("string", appCode));

            crs = QueryAdapter.QueryExecute(sqlCommand, listParam, "GetDbID", user);
            while (crs.next()) {
                mdlDB.db_id = crs.getString("db_id");
                mdlDB.app_name = appCode;
                mdlDB.is_active  = crs.getString("is_active");
                mdlDB.expired_date = crs.getString("expired_date");
            }
        } catch (Exception ex) {
            LogAdapter.InsertLogExc(ex.toString(), "GetDbID", sqlCommand, user);
            mdlDB = null;
        }

        return mdlDB;
    }

    public static model.Encryption.mdlKey DecryptClientKeys(model.Encryption.mdlKey mdlKey) {
        model.Encryption.mdlKey mdlKeyDecrypted = new model.Encryption.mdlKey();
        String appName, clientID, clientSecret, apiKey, clientIDDecrypted, clientSecretDecrypted = "";
        try {
            clientID = Base64Adapter.DecryptBase64(mdlKey.client_id);
            clientSecret = Base64Adapter.DecryptBase64(mdlKey.client_secret);
            apiKey = Base64Adapter.DecryptBase64(mdlKey.api_key);

            appName = EncryptAdapter.decrypt(clientID, "Invent123");
            clientIDDecrypted = EncryptAdapter.decrypt(clientSecret, appName);
            clientSecretDecrypted = EncryptAdapter.decrypt(apiKey, clientID);

            mdlKeyDecrypted.app_name = appName;
            mdlKeyDecrypted.client_id = clientIDDecrypted;
            mdlKeyDecrypted.client_secret = clientSecretDecrypted;
            mdlKeyDecrypted.api_key = "";
        } catch (Exception ex) {
            System.out.println(ex);
        }
        return mdlKeyDecrypted;
    }
}
