package adapter;

import database.QueryAdapter;
import model.DataTable.mdlDataTableParam;
import model.Globals;
import model.Product.mdlProduct;

import javax.sql.rowset.CachedRowSet;
import java.util.ArrayList;
import java.util.List;

public class ProductAdapter {
    public static List<mdlProduct> GetProductList(mdlDataTableParam param) {

        String sql = "";
        String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();

        List<mdlProduct> mdlProductList = new ArrayList<mdlProduct>();
        List<model.Query.mdlQueryExecute> listParam = new ArrayList<model.Query.mdlQueryExecute>();
        CachedRowSet rowset = null;

        try {
            sql = "{call sp_product_get (?,?,?)}";
            listParam.add(QueryAdapter.QueryParam("int", (param.page_number - 1) * param.page_size));
            listParam.add(QueryAdapter.QueryParam("int", param.page_size));
            listParam.add(QueryAdapter.QueryParam("string", param.search));
            rowset = QueryAdapter.QueryExecuteWithDB(sql, listParam, functionName, "product", Globals.db_name);

            while (rowset.next()) {
                mdlProduct _mdlProduct = new mdlProduct();
                _mdlProduct.id = rowset.getString("id");
                _mdlProduct.nama_barang = rowset.getString("nama_barang");
                _mdlProduct.kode_barang = rowset.getString("kode_barang");
                _mdlProduct.jumlah_barang = rowset.getString("jumlah_barang");
                _mdlProduct.tanggal = rowset.getString("tanggal");

                mdlProductList.add(_mdlProduct);
            }
        } catch (Exception ex) {
            mdlProductList = null;
            core.LogAdapter.InsertLogExc(ex.toString(), "GetProduct", sql, "product");
        }
        return mdlProductList;
    }

    public static int GetTotalProduct(mdlDataTableParam param){
        int return_value = 0;
        String sql = "";
        String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();

        List<mdlProduct> mdlProductList = new ArrayList<mdlProduct>();
        List<model.Query.mdlQueryExecute> listParam = new ArrayList<model.Query.mdlQueryExecute>();
        CachedRowSet rowset = null;

        try {
            sql = "{call sp_product_total_get (?)}";
            listParam.add(QueryAdapter.QueryParam("string", param.search));
            rowset = QueryAdapter.QueryExecuteWithDB(sql, listParam, functionName, "product", Globals.db_name);

            while (rowset.next()) {
                return_value = rowset.getInt("total");
            }
        } catch (Exception ex) {
            return_value = 0;
            core.LogAdapter.InsertLogExc(ex.toString(), "GetCompany", sql, "product");
        }
        return return_value;
    }
}
