package database;

import core.LogAdapter;
import model.Query.mdlQueryExecute;

import javax.naming.InitialContext;
import javax.sql.DataSource;
import javax.sql.rowset.CachedRowSet;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.ArrayList;
import java.util.List;

public class DatabaseAdapter {
	public static Connection getConnection_broken() throws Exception {
		try {
			String driver = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
			Class.forName(driver);
			String url = "jdbc:mysql:local_host;databaseName=invent";
			Connection conn = DriverManager.getConnection(url, "root", "admin");
			return conn;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static Connection getConnectionMultiDB_broken(String dbName) throws Exception {
		try {
			//String driver = "com.microsoft.jdbc.sqlserver.SQLServerDriver";
			String driver = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
			jdbc:sqlserver://server:port;DatabaseName=dbname
			Class.forName(driver);
			String url = "jdbc:sqlserver://35.240.192.212;databaseName="+dbName+"";
			Connection conn = DriverManager.getConnection(url, "sa", "Invent123");
			return conn;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static Connection getConnectionMultiDB(String dbName) throws Exception {
		try {
			String driver = "com.mysql.jdbc.Driver";
			Class.forName(driver);
			String url = "jdbc:mysql://localhost:3306;databaseName=" + dbName;
			Connection conn = DriverManager.getConnection(url, "root", "admin");
			return conn;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static Connection getConnection() throws Exception {
		try {
			String driver = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
			Class.forName(driver);
			String url = "jdbc:sqlserver://35.240.192.212:1433;databaseName=db_invent_management";
			Connection conn = DriverManager.getConnection(url, "sa", "Invent123");
			return conn;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static Connection getConnection_v1() throws Exception {
		try {
		    DataSource dataSource = (DataSource) new InitialContext().lookup("java:/comp/env/jdbc/db_invent_management");
		    return dataSource.getConnection();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static Connection getConnectionMultiDB_v1(String dbName) throws Exception {
		try {
			DataSource dataSource = (DataSource) new InitialContext().lookup("java:/comp/env/jdbc/"+dbName);
			return dataSource.getConnection();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static model.Client.mdlDB GetDbID(String clientID, String appCode, String user) {
		model.Client.mdlDB mdlDB = new model.Client.mdlDB();
		List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
		CachedRowSet crs = null;
		String sqlCommand = "";
		try {
			sqlCommand = "{call sp_client_get(?,?)}";
			listParam.add(QueryAdapter.QueryParam("string", clientID));
			listParam.add(QueryAdapter.QueryParam("string", appCode));

			crs = QueryAdapter.QueryExecute(sqlCommand, listParam, "mdlDB", user);
			while (crs.next()) {
				mdlDB.app_name = appCode;
				mdlDB.db_id = crs.getString("client_id");
				mdlDB.is_active = crs.getString("is_active");
				mdlDB.expired_date = crs.getString("expired_date");
			}
		} catch (Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "GetDbID", sqlCommand, user);
			mdlDB = null;
		}

		return mdlDB;
	}
}
