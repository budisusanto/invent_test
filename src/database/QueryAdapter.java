package database;

import helper.SqlImportHelper;
import model.Query.mdlResponseQuery;

import javax.sql.rowset.CachedRowSet;
import javax.sql.rowset.RowSetFactory;
import javax.sql.rowset.RowSetProvider;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class QueryAdapter {

	public static boolean QueryManipulateWithDB(String sql, List<model.Query.mdlQueryExecute> queryParam, String function, String user,String db){
		Connection connection = null;
		PreparedStatement pstm = null;
		boolean success = false;

		try{
			connection = DatabaseAdapter.getConnectionMultiDB(db);
			if (connection != null) {
				System.out.println("Connection success. Connection used for "+function+" by "+user);
				//pstm = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
				pstm = connection.prepareStatement(sql);
				if (queryParam.size() >= 0){
					for (int i=1;i<=queryParam.size();i++){
						switch (queryParam.get(i-1).paramType.toLowerCase()) {
							case "string":
								pstm.setString(i, (String) queryParam.get(i-1).paramValue);
								break;
							case "int":
								pstm.setInt(i, (int) queryParam.get(i-1).paramValue);
								break;
							case "decimal":
							case "double":
								pstm.setDouble(i, (double) queryParam.get(i-1).paramValue);
								break;
							case "boolean":
								pstm.setBoolean(i, (boolean) queryParam.get(i-1).paramValue);
								break;
							case "null":
								pstm.setNull(i, java.sql.Types.NULL);
								break;
							default:
								pstm.setString(i, (String) queryParam.get(i-1).paramValue);
								break;
						}
					}
				}
				success = pstm.execute();
				success = true;
			}
		}catch(Exception ex){
			System.out.println(ex.getMessage());
			core.LogAdapter.InsertLogExcWithDB(ex.toString(), function, pstm.toString() , user, db);
		}finally{
			try{
				if (pstm != null) pstm.close();
				if (connection != null) connection.close();
			}catch(Exception e){
				System.out.println(e.getMessage());
				core.LogAdapter.InsertLogExc(e.toString(), function, "close opened connection protocol" , user);
			}
		}
		return success;
	}

	public static mdlResponseQuery QueryManipulateWithDB2(String sql, List<model.Query.mdlQueryExecute> queryParam, String function, String user, String db){
		Connection connection = null;
		PreparedStatement pstm = null;

		mdlResponseQuery _mdlResponseQuery = new mdlResponseQuery();
		boolean success = false;

		try{
			connection = DatabaseAdapter.getConnectionMultiDB(db);
			if (connection != null) {
				System.out.println("Connection success. Connection used for "+function+" by "+user);
				//pstm = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
				pstm = connection.prepareStatement(sql);
				if (queryParam.size() >= 0){
					for (int i=1;i<=queryParam.size();i++){
						switch (queryParam.get(i-1).paramType.toLowerCase()) {
							case "string":
								pstm.setString(i, (String) queryParam.get(i-1).paramValue);
								break;
							case "int":
								pstm.setInt(i, (int) queryParam.get(i-1).paramValue);
								break;
							case "decimal":
							case "double":
								pstm.setDouble(i, (double) queryParam.get(i-1).paramValue);
								break;
							case "boolean":
								pstm.setBoolean(i, (boolean) queryParam.get(i-1).paramValue);
								break;
							case "null":
								pstm.setNull(i, java.sql.Types.NULL);
								break;
							default:
								pstm.setString(i, (String) queryParam.get(i-1).paramValue);
								break;
						}
					}
				}
				success = pstm.execute();
				success = true;

				_mdlResponseQuery.Status = success;
				_mdlResponseQuery.Response = "";
			}
		}
		catch(SQLException exs) {
			_mdlResponseQuery.Status = success;
			_mdlResponseQuery.Response = SqlImportHelper.ErrorMessageToMsg(exs.getMessage());
		}
		catch(Exception ex){
			System.out.println(ex.getMessage());
			core.LogAdapter.InsertLogExcWithDB(ex.toString(), function, pstm.toString() , user, db);
		}finally{
			try{
				if (pstm != null) pstm.close();
				if (connection != null) connection.close();
			}catch(Exception e){
				System.out.println(e.getMessage());
				core.LogAdapter.InsertLogExc(e.toString(), function, "close opened connection protocol" , user);
			}
		}
		return _mdlResponseQuery;
	}

	public static CachedRowSet QueryExecuteWithDB(String sql, List<model.Query.mdlQueryExecute> queryParam, String function, String user,String db){
		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		CachedRowSet crs = null;
		RowSetFactory rowSetFactory = null;

		try{
			connection = DatabaseAdapter.getConnectionMultiDB(db);
			if (connection != null) {
				System.out.println("Connection success. Connection used for "+function+" by "+user);
				System.out.println("Query: "+sql);
//				pstm = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
				pstm = connection.prepareStatement(sql, 1004, 1007);
				if (queryParam.size() >= 0){
					for (int i=1;i<=queryParam.size();i++){
						System.out.println("Parameter "+i+" : "+queryParam.get(i-1).paramValue+" || Type : "+queryParam.get(i-1).paramType.toLowerCase());

						switch (queryParam.get(i-1).paramType.toLowerCase()) {
							case "string":
								pstm.setString(i, (String) queryParam.get(i-1).paramValue);
								break;
							case "int":
								pstm.setInt(i, (int) queryParam.get(i-1).paramValue);
								break;
							case "decimal":
							case "double":
								pstm.setDouble(i, (double) queryParam.get(i-1).paramValue);
								break;
							case "boolean":
								pstm.setBoolean(i, (boolean) queryParam.get(i-1).paramValue);
								break;
							case "null":
								pstm.setNull(i, java.sql.Types.NULL);
								break;
							default:
								pstm.setString(i, (String) queryParam.get(i-1).paramValue);
								break;
						}
					}
				}

				rs = pstm.executeQuery();
				System.out.println("Total number of Row: "+getNumOfRows(rs)+" Function: "+function);
				System.out.println();
				rowSetFactory = RowSetProvider.newFactory();
				crs = rowSetFactory.createCachedRowSet();
				crs.populate(rs);
			}
		}catch(Exception ex){
			System.out.println(ex.getMessage());
			core.LogAdapter.InsertLogExc(ex.toString(), function, pstm.toString() , user);
		}finally{
			try{
				if (pstm != null) pstm.close();
				if (rs != null) rs.close();
				if (connection != null) connection.close();
			}catch(Exception e){
				System.out.println(e.getMessage());
				core.LogAdapter.InsertLogExcWithDB(e.toString(), function, "close opened connection protocol" , user, db);
			}
		}
		return crs;
	}

	public static CachedRowSet QueryExecuteWithDB_V2(String sql, List<model.Query.mdlQueryExecute> queryParam, String function, String user,String db){
		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		CachedRowSet crs = null;
		RowSetFactory rowSetFactory = null;

		try{
			connection = DatabaseAdapter.getConnectionMultiDB(db);
			if (connection != null) {
				System.out.println("Connection success. Connection used for "+function+" by "+user);
				System.out.println("Query: "+sql);
				pstm = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);

				if (queryParam.size() >= 0){
					for (int i=1;i<=queryParam.size();i++){
						System.out.println("Parameter "+i+" : "+queryParam.get(i-1).paramValue+" || Type : "+queryParam.get(i-1).paramType.toLowerCase());

						switch (queryParam.get(i-1).paramType.toLowerCase()) {
							case "string":
								pstm.setString(i, (String) queryParam.get(i-1).paramValue);
								break;
							case "int":
								pstm.setInt(i, (int) queryParam.get(i-1).paramValue);
								break;
							case "decimal":
							case "double":
								pstm.setDouble(i, (double) queryParam.get(i-1).paramValue);
								break;
							case "boolean":
								pstm.setBoolean(i, (boolean) queryParam.get(i-1).paramValue);
								break;
							case "null":
								pstm.setNull(i, java.sql.Types.NULL);
								break;
							default:
								pstm.setString(i, (String) queryParam.get(i-1).paramValue);
								break;
						}
					}
				}

				rs = pstm.executeQuery();
				System.out.println("Total number of Row: "+getNumOfRows(rs)+" Function: "+function);
				System.out.println();
				rowSetFactory = RowSetProvider.newFactory();
				crs = rowSetFactory.createCachedRowSet();
				crs.populate(rs);
			}
		}catch(Exception ex){
			System.out.println(ex.getMessage());
			core.LogAdapter.InsertLogExc(ex.toString(), function, pstm.toString() , user);
		}finally{
			try{
				if (pstm != null) pstm.close();
				if (rs != null) rs.close();
				if (connection != null) connection.close();
			}catch(Exception e){
				System.out.println(e.getMessage());
				core.LogAdapter.InsertLogExcWithDB(e.toString(), function, "close opened connection protocol" , user, db);
			}
		}
		return crs;
	}

	public static CachedRowSet QueryExecute(String sql, List<model.Query.mdlQueryExecute> queryParam, String function, String user){
		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		CachedRowSet crs = null;
		RowSetFactory rowSetFactory = null;
		
		try{
			connection = DatabaseAdapter.getConnection();
			if (connection != null) {
				System.out.println("Connection success. Connection used for "+function+" by "+user);
				System.out.println("Query: "+sql);
				pstm = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);

				if (queryParam.size() >= 0){
					for (int i=1;i<=queryParam.size();i++){
						System.out.println("Parameter "+i+" : "+queryParam.get(i-1).paramValue+" || Type : "+queryParam.get(i-1).paramType.toLowerCase());
						
						switch (queryParam.get(i-1).paramType.toLowerCase()) {
							case "string":
								pstm.setString(i, (String) queryParam.get(i-1).paramValue);
								break;
							case "int":
								pstm.setInt(i, (int) queryParam.get(i-1).paramValue);
								break;
							case "decimal":
							case "double":
								pstm.setDouble(i, (double) queryParam.get(i-1).paramValue);
								break;
							case "boolean":
								pstm.setBoolean(i, (boolean) queryParam.get(i-1).paramValue);
								break;
							case "null":
								pstm.setNull(i, java.sql.Types.NULL);
							break;
							default: 
								pstm.setString(i, (String) queryParam.get(i-1).paramValue);
								break;
						}
					}
				}
				
				rs = pstm.executeQuery();
				System.out.println("Total number of Row: "+getNumOfRows(rs)+" Function: "+function);
				System.out.println();
				rowSetFactory = RowSetProvider.newFactory();
				crs = rowSetFactory.createCachedRowSet();
				crs.populate(rs);
			}
		}catch(Exception ex){
			System.out.println(ex.getMessage());
			core.LogAdapter.InsertLogExc(ex.toString(), function, pstm.toString() , user);
		}finally{
			try{
				if (pstm != null) pstm.close();
				if (rs != null) rs.close();
				if (connection != null) connection.close();
			}catch(Exception e){
				System.out.println(e.getMessage());
				core.LogAdapter.InsertLogExc(e.toString(), function, "close opened connection protocol" , user);
			}
		}
		return crs;
	}
	
	public static int getNumOfRows(ResultSet res) throws SQLException{
		int currentRow = res.getRow();
		int counter = res.last() ? res.getRow() : 0;
		if (currentRow == 0) {
			res.beforeFirst();
		}
		else {
			res.absolute(currentRow);
		}
		return counter;
	}

	public static boolean QueryManipulateNoLog(String sql, List<model.Query.mdlQueryExecute> queryParam, String function, String user){ //* Func specially for upload query log exception

		Connection connection = null;
		PreparedStatement pstm = null;
		boolean success = false;

		try{
			connection = DatabaseAdapter.getConnection();
			if (connection != null) {
				System.out.println("Connection success. Connection used for "+function+" by "+user);
				//pstm = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
				pstm = connection.prepareStatement(sql);
				
				if (queryParam.size() >= 0){
					for (int i=1;i<=queryParam.size();i++){
						switch (queryParam.get(i-1).paramType.toLowerCase()) {
						case "string":
							pstm.setString(i, (String) queryParam.get(i-1).paramValue);
							break;
						case "int":
							pstm.setInt(i, (int) queryParam.get(i-1).paramValue);
							break;
						case "decimal":
						case "double":
							pstm.setDouble(i, (double) queryParam.get(i-1).paramValue);
							break;
						case "boolean":
							pstm.setBoolean(i, (boolean) queryParam.get(i-1).paramValue);
							break;
						case "null":
							pstm.setNull(i, java.sql.Types.NULL);
							break;
						default: 
							pstm.setString(i, (String) queryParam.get(i-1).paramValue);
							break;
						}
					}
				}
				success = pstm.execute();
			}
		}catch(Exception ex){
			System.out.println(ex.getMessage());
			//adapter.LogAdapter.InsertLogExc(ex.toString(), function, pstm.toString() , user);
		}finally{
			try{
				if (pstm != null) pstm.close();
				if (connection != null) connection.close();
			}catch(Exception e){
				System.out.println(e.getMessage());
				//adapter.LogAdapter.InsertLogExc(e.toString(), function, "close opened connection protocol" , user);
			}
		}
		return success;
	}

	public static boolean QueryManipulateNoLogWithDB(String sql, List<model.Query.mdlQueryExecute> queryParam, String function, String user, String db){ //* Func specially for upload query log exception

		Connection connection = null;
		PreparedStatement pstm = null;
		boolean success = false;

		try{
			connection = DatabaseAdapter.getConnectionMultiDB(db);
			if (connection != null) {
				System.out.println("Connection success. Connection used for "+function+" by "+user);
				//pstm = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
				pstm = connection.prepareStatement(sql);

				if (queryParam.size() >= 0){
					for (int i=1;i<=queryParam.size();i++){
						switch (queryParam.get(i-1).paramType.toLowerCase()) {
							case "string":
								pstm.setString(i, (String) queryParam.get(i-1).paramValue);
								break;
							case "int":
								pstm.setInt(i, (int) queryParam.get(i-1).paramValue);
								break;
							case "decimal":
							case "double":
								pstm.setDouble(i, (double) queryParam.get(i-1).paramValue);
								break;
							case "boolean":
								pstm.setBoolean(i, (boolean) queryParam.get(i-1).paramValue);
								break;
							case "null":
								pstm.setNull(i, java.sql.Types.NULL);
								break;
							default:
								pstm.setString(i, (String) queryParam.get(i-1).paramValue);
								break;
						}
					}
				}
				success = pstm.execute();
			}
		}catch(Exception ex){
			System.out.println(ex.getMessage());
			//adapter.LogAdapter.InsertLogExc(ex.toString(), function, pstm.toString() , user);
		}finally{
			try{
				if (pstm != null) pstm.close();
				if (connection != null) connection.close();
			}catch(Exception e){
				System.out.println(e.getMessage());
				//adapter.LogAdapter.InsertLogExc(e.toString(), function, "close opened connection protocol" , user);
			}
		}
		return success;
	}

	public static model.Query.mdlQueryExecute QueryParam(String type, Object value){
		model.Query.mdlQueryExecute param = new model.Query.mdlQueryExecute();
		param.paramType = type;
		param.paramValue = value;

		return param;
	}

	public static Boolean QueryTransaction(List<model.Query.mdlQueryTransaction> listMdlQueryTrans, String function, String user){
		Connection connection = null;
		PreparedStatement pstm = null;
		Boolean success = false;
		String sql;
		List<model.Query.mdlQueryExecute> queryParam;

		try{
			connection = DatabaseAdapter.getConnection();
			if (connection != null) {
				connection.setAutoCommit(false);
				
				for(model.Query.mdlQueryTransaction mdlQueryTrans : listMdlQueryTrans){
//					System.out.println("ParamRowSize: "+listMdlQueryTrans.size());
					pstm = null;
					sql = mdlQueryTrans.sql;
					queryParam = new ArrayList<model.Query.mdlQueryExecute>();
					queryParam.addAll(mdlQueryTrans.list_param);
					//pstm = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
					pstm = connection.prepareStatement(sql);
//					System.out.println("ParamColSize: "+queryParam.size());
					for (int i=1;i<=queryParam.size();i++){
						switch (queryParam.get(i-1).paramType.toLowerCase()) {
						case "string":
							pstm.setString(i, (String) queryParam.get(i-1).paramValue);
							break;
						case "int":
							pstm.setInt(i, (int) queryParam.get(i-1).paramValue);
							break;
						case "decimal":
						case "double":
							pstm.setDouble(i, (double) queryParam.get(i-1).paramValue);
							break;
						case "boolean":
							pstm.setBoolean(i, (boolean) queryParam.get(i-1).paramValue);
							break;
						case "null":
							pstm.setNull(i, java.sql.Types.NULL);
							break;
						default: 
							pstm.setString(i, (String) queryParam.get(i-1).paramValue);
							break;
						}
					}
	
					pstm.executeUpdate();
				}
	
				connection.commit();
				success = true;
			}
		}catch(Exception ex){
			if (connection != null) {
				try {
					System.err.print("Transaction is being rolled back");
					connection.rollback();
				} catch(SQLException excep) {
					core.LogAdapter.InsertLogExc(excep.toString(), "Transaction"+function, pstm.toString() , user);
				}
			}
			System.out.println(ex.getMessage());
			core.LogAdapter.InsertLogExc(ex.toString(), function, pstm.toString() , user);
			success = false;
		}finally{
			try{
				connection.setAutoCommit(true);
				if (pstm != null) pstm.close();
				if (connection != null) connection.close();
			}catch(Exception e){
				System.out.println(e.getMessage());
				core.LogAdapter.InsertLogExc(e.toString(), function, "close opened connection protocol" , user);
			}
		}

		return success;
	}

	public static Boolean QueryTransactionWithDB(List<model.Query.mdlQueryTransaction> listMdlQueryTrans, String function, String user, String db ){
		Connection connection = null;
		PreparedStatement pstm = null;
		Boolean success = false;
		String sql;
		List<model.Query.mdlQueryExecute> queryParam;

		try{
			connection = DatabaseAdapter.getConnectionMultiDB(db);
			if (connection != null) {
				connection.setAutoCommit(false);

				for(model.Query.mdlQueryTransaction mdlQueryTrans : listMdlQueryTrans){
//					System.out.println("ParamRowSize: "+listMdlQueryTrans.size());
					pstm = null;
					sql = mdlQueryTrans.sql;
					queryParam = new ArrayList<model.Query.mdlQueryExecute>();
					queryParam.addAll(mdlQueryTrans.list_param);
					//pstm = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
					pstm = connection.prepareStatement(sql);
//					System.out.println("ParamColSize: "+queryParam.size());
					for (int i=1;i<=queryParam.size();i++){
						switch (queryParam.get(i-1).paramType.toLowerCase()) {
							case "string":
								pstm.setString(i, (String) queryParam.get(i-1).paramValue);
								break;
							case "int":
								pstm.setInt(i, (int) queryParam.get(i-1).paramValue);
								break;
							case "decimal":
							case "double":
								pstm.setDouble(i, (double) queryParam.get(i-1).paramValue);
								break;
							case "boolean":
								pstm.setBoolean(i, (boolean) queryParam.get(i-1).paramValue);
								break;
							case "null":
								pstm.setNull(i, java.sql.Types.NULL);
								break;
							default:
								pstm.setString(i, (String) queryParam.get(i-1).paramValue);
								break;
						}
					}

					pstm.executeUpdate();
				}

				connection.commit();
				success = true;
			}
		}catch(Exception ex){
			if (connection != null) {
				try {
					System.err.print("Transaction is being rolled back");
					connection.rollback();
				} catch(SQLException excep) {
					core.LogAdapter.InsertLogExc(excep.toString(), "Transaction"+function, pstm.toString() , user);
				}
			}
			System.out.println(ex.getMessage());
			core.LogAdapter.InsertLogExc(ex.toString(), function, pstm.toString() , user);
			success = false;
		}finally{
			try{
				connection.setAutoCommit(true);
				if (pstm != null) pstm.close();
				if (connection != null) connection.close();
			}catch(Exception e){
				System.out.println(e.getMessage());
				core.LogAdapter.InsertLogExcWithDB(e.toString(), function, "close opened connection protocol" , user,db);
			}
		}

		return success;
	}

	public static boolean QueryManipulate(String sql, List<model.Query.mdlQueryExecute> queryParam, String function, String user){
		Connection connection = null;
		PreparedStatement pstm = null;
		boolean success = false;

		try{
			connection = DatabaseAdapter.getConnection();
			if (connection != null) {
				System.out.println("Connection success. Connection used for "+function+" by "+user);
				//pstm = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
				pstm = connection.prepareStatement(sql);
				if (queryParam.size() >= 0){
					for (int i=1;i<=queryParam.size();i++){
						switch (queryParam.get(i-1).paramType.toLowerCase()) {
							case "string":
								pstm.setString(i, (String) queryParam.get(i-1).paramValue);
								break;
							case "int":
								pstm.setInt(i, (int) queryParam.get(i-1).paramValue);
								break;
							case "decimal":
							case "double":
								pstm.setDouble(i, (double) queryParam.get(i-1).paramValue);
								break;
							case "boolean":
								pstm.setBoolean(i, (boolean) queryParam.get(i-1).paramValue);
								break;
							case "null":
								pstm.setNull(i, java.sql.Types.NULL);
								break;
							default:
								pstm.setString(i, (String) queryParam.get(i-1).paramValue);
								break;
						}
					}
				}
				success = pstm.execute();
			}
		}catch(Exception ex){
			System.out.println(ex.getMessage());
			core.LogAdapter.InsertLogExc(ex.toString(), function, pstm.toString() , user);
		}finally{
			try{
				if (pstm != null) pstm.close();
				if (connection != null) connection.close();
			}catch(Exception e){
				System.out.println(e.getMessage());
				core.LogAdapter.InsertLogExc(e.toString(), function, "close opened connection protocol" , user);
			}
		}
		return success;
	}

	public static CachedRowSet QueryExecute(String sql, String function, String user){
		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		CachedRowSet crs = null;
		RowSetFactory rowSetFactory = null;

		try{
			connection = DatabaseAdapter.getConnection();
			if (connection != null) {
				System.out.println("Connection success. Connection used for "+function+" by "+user);
				System.out.println("Query: "+sql);
				pstm = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
				rs = pstm.executeQuery();
				System.out.println("Total number of Row: "+getNumOfRows(rs)+" Function: "+function);
				System.out.println();
				rowSetFactory = RowSetProvider.newFactory();
				crs = rowSetFactory.createCachedRowSet();
				crs.populate(rs);
			}
		}catch(Exception ex){
			System.out.println(ex.getMessage());
			core.LogAdapter.InsertLogExc(ex.toString(), function, pstm.toString() , user);
		}finally{
			try{
				if (pstm != null) pstm.close();
				if (rs != null) rs.close();
				if (connection != null) connection.close();
			}catch(Exception e){
				System.out.println(e.getMessage());
				core.LogAdapter.InsertLogExc(e.toString(), function, "close opened connection protocol" , user);
			}

		}
		return crs;
	}

}